import MobileMenu from "./mobile-menu";
import MobileMenuButton from "./mobine-menu-button";
import { useState } from "react";
import LogoCommon from "../common/logo";
// import { Popover } from "antd";
import { Link as LinkScroll } from "react-scroll";

export default function Header() {
  const [menuState, setMenu] = useState(false);

  const [activeLink, setActiveLink] = useState(null);

  const toggleMenu = () => {
    setMenu(!menuState);
  };

  // const content = <p className="text-[16px]">Тун удахгүй</p>;
  return (
    <header className="header shadow-md">
      <div>
        <nav className="nav">
          <LogoCommon />
        </nav>
        <div className="flex-action">
          <LinkScroll
            activeClass="active"
            to="about"
            spy={true}
            smooth={true}
            duration={1000}
            onSetActive={() => {
              setActiveLink("about");
            }}
            className={
              "px-4 py-2 mx-2 cursor-pointer animation-hover inline-block text-white relative" +
              (activeLink === "about"
                ? " text-[#f58521] animation-active"
                : " text-black-500 text-[16px] hover:text-[#f58521]")
            }
          >
            Танилцуулга
          </LinkScroll>

          <LinkScroll
            activeClass="active"
            to="step"
            spy={true}
            smooth={true}
            duration={1000}
            onSetActive={() => {
              setActiveLink("step");
            }}
            className={
              "px-4 py-2 mx-2 cursor-pointer animation-hover text-white inline-block relative" +
              (activeLink === "step"
                ? " text-[#f58521] animation-active"
                : " text-black-500 text-[16px] hover:text-[#f58521]")
            }
          >
            Хөрөнгө оруулагч
          </LinkScroll>
          {/* <Popover content={content}> */}
          <button className="mx-2 bg-[#f584213e] hover:text-[divide-gray-100] text-white border-[#f584213e] lg:text-[16px] md:text-[10px]  rounded-[8px]  px-[16px] text-center py-[12px]   font-bold  hover:bg-[#f58521] hover:ring-sky-[#f58521] ">
            Нэвтрэх
          </button>
          {/* </Popover> */}
        </div>
        {/* <!-- mobile menu button --> */}
        <MobileMenuButton menuState={menuState} togMenu={toggleMenu} />
        {/* <!-- mobile menu --> */}
        {menuState ? <MobileMenu /> : null}
      </div>
    </header>
  );
}
